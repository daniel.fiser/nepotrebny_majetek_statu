# Nepotřebný majetek státu Alert Service
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This is a simple Python 3.6 mailer for Czech government open data listing
unused state property for sale.

See
[https://data.gov.cz/datov%C3%A1-sada?iri=https%3A%2F%2Fdata.gov.cz%2Fzdroj%2Fdatov%C3%A9-sady%2FMV%2F143188351](https://data.gov.cz/datov%C3%A1-sada?iri=https%3A%2F%2Fdata.gov.cz%2Fzdroj%2Fdatov%C3%A9-sady%2FMV%2F143188351)
for more details of the open data format, URLs, etc.

The mailer fetches all new entries that were published yesterday, filters them
by a user set criteria, generates a HTML message with the entries, and sends
the message to specified recipients.

## Setup

The application consists of two files; both are needed for it to run:

- `nms.py` -- the actual script
- `nms.conf` -- its configuration; note that the config file can have a different name, but must be specified as an argument to the script

See the sample `nms.sample.conf`, edit it to match your settings,
and save it as `nms.conf`.

### Docker container
The script can be built and distributed as a Docker image.

#### Build and distribute
````
REGISTRY_HANDLE=registry.gitlab.com/daniel.fiser/nepotrebny_majetek_statu
sudo docker build -t ${REGISTRY_HANDLE} .
sudo docker push ${REGISTRY_HANDLE}
````

#### Deploy to a remote machine
````
REGISTRY_HANDLE=registry.gitlab.com/daniel.fiser/nepotrebny_majetek_statu
/usr/bin/docker image pull ${REGISTRY_HANDLE}
/usr/bin/docker run --mount=type=bind,source=<your config dir>,destination=/usr/src/app/etc --rm --name nms ${REGISTRY_HANDLE}
````

## Execution
The script accepts one optional argument: path to the config file. If not
specified, it searches for `nms.conf` in the directory with the script
itself.

It's recommended to use it from cron to get notified about new notices daily.
If executed as a Docker container, please make sure your user has privileges to exec docker and that a directory with config file is bound to the container.

````crontab
05 07 * * * /usr/bin/docker run --mount=type=bind,source=/docker/nms,destination=/usr/src/app/etc --rm --name nms registry.gitlab.com/daniel.fiser/nepotrebny_majetek_statu -c /usr/src/app/etc/nms.conf
````

# Licence
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

