#!/usr/bin/python3 -tt

import configparser
import getopt
import smtplib
import sys
from datetime import timedelta, date
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import access, R_OK

import requests
from lxml import etree

# URL to XML document with list of state property for sale
BASE_URL = 'https://portal.gov.cz'
NMS_NAMESPACE = '/portal/xsd/PvsRejstrikData.xsd'
NMS_DATA = '/otevrena-data/datove-zdroje/nepotrebny-majetek.xml'
URL = '{}{}'.format(BASE_URL, NMS_DATA)
NS = '{}{}'.format(BASE_URL, NMS_NAMESPACE)

# Exit if Python < 3.6
if sys.version_info < (3, 6):
    print('Python 3.6 or higher required, {}.{}.{} found'.format(
        sys.version_info.major,
        sys.version_info.minor,
        sys.version_info.micro))
    sys.exit(1)


# SSL e-mail sending routine.
def send_HTML_email(from_addr, to_addr_list, cc_addr_list,
                    subject, HTML,
                    login, password,
                    smtpserver):
    message = MIMEMultipart('alternative')
    message['From'] = from_addr
    message['To'] = ','.join(to_addr_list)
    message['Cc'] = ','.join(cc_addr_list)
    message['Subject'] = subject

    part1 = MIMEText(HTML, 'html', 'utf-8')
    message.attach(part1)

    # See http://pymotw.com/2/smtplib/ for proper SMTP handling, including TLS
    try:
        server = smtplib.SMTP_SSL(smtpserver, timeout=5)
        # server.set_debuglevel(1)
        server.login(login, password)
        server.sendmail(from_addr, to_addr_list, message.as_string())
        server.quit()
    except smtplib.SMTPException as err:
        print("Sending e-mail message failed!")
        print(str(err))
        sys.exit(2)


# function to convert UTF-8 text to ASCII with XML entities
def utf8_to_ascii(input: str) -> str:
    return input.encode('ascii', 'xmlcharrefreplace').decode('ascii')


# get options
try:
    opts, args = getopt.getopt(sys.argv[1:], "c:")
except getopt.GetoptError as err:
    print(str(err))
    sys.exit(2)

# check if a config file was specified as an argument
config_file = '{}/nms.conf'.format(sys.path[0])
for o, a in opts:
    if o == "-c":
        config_file = str(a)
    else:
        assert False, "unhandled option"

# is the config file readable?
if not access(config_file, R_OK):
    print('Config file is not readable: {}'.format(config_file))
    sys.exit(2)

# read credentials etc. from the config file
config = configparser.ConfigParser()
config.read_file(open(file=config_file, mode='r', encoding='utf8'))

# Get yesterday's date
yesterday = date.today() - timedelta(1)

# Get the XML document
try:
    response = requests.get(URL, headers={
        'Accept': 'application/xml',
        'User-Agent': 'nms/2019'})
except Exception as err:
    print("Error accessing %s" % URL)
    print(err)
    sys.exit(1)

# Parse the XML
if response.status_code == requests.codes.ok:
    try:
        tree = etree.fromstring(response.content)
    except Exception as err:
        print("Error parsing the XML")
        print(err)
else:
    print("Error retrieving XML from %s" % URL)
    sys.exit(1)

# Get all documents for the requested notice board(s)
xmlns = {'nm': NS}
records = tree.findall('./nm:Zaznam[@zverejneno="{}"]'
        .format(yesterday.isoformat()), xmlns)

# find and store all documents matching search criteria
selected_records = []
for r in records:
    if config['nms']['search_string'] in \
        r.find('.//nm:Atributy/nm:Atribut[@nazev="{}"]'
                .format(config['nms']['search_attribute']),
                xmlns).text:
        selected_records.append(r)

doc_count = len(selected_records)
if doc_count > 0:
    html = """\
<html>
  <head></head>
  <body>
    <h1>Nové nepotřebné nemovitosti státu {}: {}</h1>
    <ol>
""".format(config['nms']['search_string'], doc_count)

    # process all documents
    for r in selected_records:
        html += "<li><a href='{}'>KÚ {}: {}</a> " \
                "<em>(zveřejněno {})</em>\n".format(
            BASE_URL + r.attrib['url'],
            utf8_to_ascii(r.find('.//nm:Atributy/nm:Atribut[@nazev="KATASTRALNI_UZEMI"]',
                xmlns).text),
            utf8_to_ascii(r.find('.//nm:Atributy/nm:Atribut[@nazev="POPIS"]',
                xmlns).text),
            r.attrib['zverejneno'])
        html += '</li>\n'

    html += """\
    </ol>
  </body>
</html>
"""

    send_HTML_email(from_addr='Hlídač úřední desky <{}>'.format(
        config['SMTP']['login']),
        smtpserver=config['SMTP']['server'],
        login=config['SMTP']['login'],
        password=config['SMTP']['password'],
        to_addr_list=config['SMTP']['to'].split(','),
        cc_addr_list=[],
        subject='Nové nepotřebné nemovitosti státu {}: {}'.format(
            config['nms']['search_string'],
            doc_count),
        HTML=html)
