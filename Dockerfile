FROM python:3.6-alpine

MAINTAINER Daniel Fišer <daniel@fiser.cz>

WORKDIR /usr/src/app
RUN mkdir -p /usr/src/app/etc

COPY nms.py README.md requirements.txt ./
COPY nms.sample.conf /usr/src/app/etc/

# Install packages required to compile Python3 lxml
RUN apk --update --no-cache add gcc musl-dev libxslt-dev \
	&& pip3 install --no-cache-dir -r requirements.txt \
	&& apk --update --no-cache del gcc musl-dev \
	&& rm -rf /var/cache/apk/*

# Expose a config directory
VOLUME ["/usr/src/app/etc"]

ENTRYPOINT ["python", "./nms.py"]
CMD [""]

